<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS clubs (
                id int(11) UNIQUE NOT NULL AUTO_INCREMENT,
                name varchar(20) NOT NULL UNIQUE,
                PRIMARY KEY (id)
            );
        ";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = "DROP TABLE IF EXISTS clubs;";
        DB::statement($query);
    }
}
