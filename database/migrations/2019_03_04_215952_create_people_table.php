<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS people (
                id int(11) UNIQUE NOT NULL AUTO_INCREMENT,
                name varchar(20),
                sex enum('male', 'female'),
                club_id int(11),
                PRIMARY KEY (id),
                FOREIGN KEY (club_id) REFERENCES clubs (id) ON DELETE SET NULL
            );
        ";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = "DROP TABLE IF EXISTS people;";
        DB::statement($query);
    }
}
