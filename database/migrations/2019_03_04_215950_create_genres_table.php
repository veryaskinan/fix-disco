<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS genres (
                id int(11) UNIQUE NOT NULL AUTO_INCREMENT,
                name varchar(20) UNIQUE NOT NULL,
                PRIMARY KEY (id)
            );
        ";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = "DROP TABLE IF EXISTS genres;";
        DB::statement($query);
    }
}
