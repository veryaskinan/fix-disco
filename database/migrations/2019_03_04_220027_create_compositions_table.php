<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateCompositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS compositions (
                id int(11) UNIQUE NOT NULL AUTO_INCREMENT,
                name varchar(20),
                genre_id int(11),
                romantic boolean,
                PRIMARY KEY (id),
                FOREIGN KEY (genre_id) REFERENCES genres (id)  ON DELETE CASCADE
            );
        ";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = "DROP TABLE IF EXISTS compositions;";
        DB::statement($query);
    }
}
