<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateMtmClubsCompositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "
            CREATE TABLE IF NOT EXISTS mtm_clubs_compositions (
                club_id int(11),
                composition_id int(11),
                PRIMARY KEY (club_id, composition_id),
                FOREIGN KEY (club_id) REFERENCES clubs (id) ON DELETE CASCADE,
                FOREIGN KEY (composition_id) REFERENCES compositions (id) ON DELETE CASCADE
            );
        ";
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = "DROP TABLE IF EXISTS mtm_clubs_compositions;";
        DB::statement($query);
    }
}
