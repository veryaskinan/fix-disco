<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('clubs/get', 'ClubController@get');
Route::post('clubs/add', 'ClubController@add');
Route::post('clubs/remove', 'ClubController@remove');
Route::post('clubs/add-music', 'ClubController@addMusic');
Route::post('clubs/remove-music', 'ClubController@removeMusic');
Route::post('clubs/play-music', 'ClubController@playMusic');

Route::post('genres/get', 'GenreController@get');
Route::post('genres/add', 'GenreController@add');
Route::post('genres/remove', 'GenreController@remove');

Route::post('compositions/get', 'CompositionController@get');
Route::post('compositions/add', 'CompositionController@add');
Route::post('compositions/remove', 'CompositionController@remove');
Route::post('compositions/add-to-playlist', 'CompositionController@addToPlaylist');
Route::post('compositions/remove-from-playlist', 'CompositionController@removeFromPlaylist');

Route::post('people/get', 'PeopleController@get');
Route::post('people/add', 'PeopleController@add');
Route::post('people/remove', 'PeopleController@remove');
Route::post('people/enter-club', 'PeopleController@enterClub');
Route::post('people/leave-club', 'PeopleController@leaveClub');
