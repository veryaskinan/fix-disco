<?php

namespace App\Services;

class PeopleService
{
    public static function add($peopleDataArray)
    {
        foreach ($peopleDataArray as $personData) {
            $person = new \App\Models\Person();
            $person->name = $personData['name'];
            $person->sex = $personData['sex'];
            $person->save();
            $genres = \App\Models\Genre::find($personData['genres']);
            $person->genres()->attach($genres);
        }
    }

    public static function enterClub($peopleIds, $clubId)
    {
        $people = \App\Models\Person::find($peopleIds);
        $club = \App\Models\Club::find($clubId);
        $club->guests()->saveMany($people);
    }

    public static function leaveClub($peopleIds)
    {
        $people = \App\Models\Person::find($peopleIds);
        foreach ($people as $person) {
            $person->club_id = null;
            $person->save();
        }
        return;
    }
}
