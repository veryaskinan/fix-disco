<?php

namespace App\Services;


class ClubService
{
    public function addMusicToClub($clubId, $compositionIds)
    {
        $club = \App\Models\Club::find($clubId);
        $compositions = \App\Models\Composition::find($compositionIds);
        $club->compositions()->attach($compositions);
    }

    public function removeMusicFromClub($clubId, $compositionIds)
    {
        $club = \App\Models\Club::find($clubId);
        $compositions = \App\Models\Composition::find($compositionIds);
        $club->compositions()->detach($compositions);
    }

    public function playMusic()
    {
        $danceResult = [];

        $clubs = \App\Models\Club::with(['compositions.genre', 'guests.genres'])->get();
        foreach ($clubs as $club) {
            $danceResult[$club->id] = $club->playMusic();
        }
        return $danceResult;
    }

}
