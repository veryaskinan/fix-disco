<?php

namespace App\Services;

class CompositionService
{

    public static function addToPlaylist($compostionsIds, $clubId)
    {
        $compositions = \App\Models\Composition::find($compostionsIds);
        $club = \App\Models\Club::find($clubId);
        $club->compositions()->attach($compositions);
    }

    public static function removeFromPlaylist($ompostionsIds, $clubId)
    {
        $compositions = \App\Models\Composition::find($ompostionsIds);
        $club = \App\Models\Club::find($clubId);
        $club->compositions()->detach($compositions);
    }
}
