<?php
/**
 * Created by PhpStorm.
 * User: andrei
 * Date: 02.03.19
 * Time: 17:04
 */

namespace App;


class Error
{
    public $message = 'Unknown error';

    public function __construct($message)
    {
        $this->message = $message;
    }
}
