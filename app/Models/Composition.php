<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Composition extends Model
{
    const TABLE = 'compositions';

    protected $table = self::TABLE;

    /**
     * Defines many-to-one relation with genres
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genre()
    {
        return $this->belongsTo('App\Models\Genre');
    }

    static function add (array $compositionsArray)
    {
        $result = new \stdClass();
        $result->entities = false;

        $arrayForInsert = array_map(
            function($composition) {
                return (array)$composition;
            },
            $compositionsArray
        );

        $result->success = DB::table(self::TABLE)->insert($arrayForInsert);

        return $result;
    }

    static function remove (array $compositionsArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $result->success = DB::table(self::TABLE)
            ->whereIn('id', $compositionsArray)
            ->delete()
        ;

        return $result;
    }

    public function isRomantic()
    {
        return (boolean)($this->romantic);
    }

    public function short()
    {
        $romantic = $this->isRomantic() ? 'romantic' : '';

        return (object)[
            "id" => $this->id,
            "data" => "{$this->name} ({$this->genre->name}) {$romantic}"
        ];
    }

    public function dancingFormat()
    {
        return (object)[
            'name' => $this->name,
            'genre' => $this->genre->short(),
            'romantic' => (boolean)$this->romantic,
            'dancingPeople' => $this->dancingPeople,
            'stayingPeople' => $this->stayingPeople,
        ];
    }
}
