<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    const TABLE = 'clubs';

    protected $table = self::TABLE;

    /**
     * Defines many-to-many relation with compositions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function compositions()
    {
        return $this->belongsToMany('App\Models\Composition', 'mtm_clubs_compositions');
    }

    /**
     * Defines one-to-many relation with people
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function guests()
    {
        return $this->hasMany('App\Models\Person');
    }


    static function add (array $clubsArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $arrayForInsert = array_map(
            function($club) {
                return (array)$club;
            },
            $clubsArray
        );

        $result->success = DB::table(self::TABLE)->insert($arrayForInsert);

        return $result;
    }

    static function remove (array $clubsIdsArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $result->success = DB::table(self::TABLE)
            ->whereIn('id', $clubsIdsArray)
            ->delete()
        ;

        return $result;
    }

    public function playMusic ()
    {
        $this->load('compositions.genre', 'guests.genres');
        if (count($this->compositions) > 0)
        {
            foreach ($this->compositions as $composition)
            {
                $composition->dancingPeople = [];
                $composition->stayingPeople = [];
                // filter people with genre
                foreach ($this->guests as $guest) {
                    if (count($guest->genres) > 0 && $guest->genres->contains($composition->genre)) {
                        $composition->dancingPeople = array_merge($composition->dancingPeople, [$guest->dancingFormat()]);
                    } else {
                        $composition->stayingPeople = array_merge($composition->stayingPeople, [$guest->dancingFormat()]);
                    }
                }
                // format people with romantic music
                if ($composition->isRomantic() && count($composition->dancingPeople) > 0)
                {
                    // split men from women
                    $dancingMen = [];
                    $dancingWomen = [];
                    foreach ($composition->dancingPeople as $dancingPerson) {
                        switch ($dancingPerson->sex) {
                            case 'male':
                                $dancingMen[] = $dancingPerson;
                                break;
                            case 'female':
                                $dancingWomen[] = $dancingPerson;
                                break;
                        }
                    }
                    // making couples and group
                    $couples = [];
                    while (count($dancingMen) > 0 && count($dancingWomen) > 0 ) {
                        $couples[] = [array_pop($dancingMen), array_pop($dancingWomen)];
                    }
                    $group = array_merge($dancingMen, $dancingWomen);
                    if (count($group)) {
                        $couples[] = $group;
                    }
                    $composition->dancingPeople = $couples;
                }
            }
        }

        $dancingResult = $this->dancingFormat();

        return $dancingResult;
    }

    public function short()
    {
        return (object)[
            "id" => $this->id,
            "data" => $this->name
        ];
    }

    public function dancingFormat()
    {
        return (object)[
            'name' => $this->name,
            'compositions' => array_map(
                function($composition) {
                    return $composition->dancingFormat();
                },
                $this->compositions->all()
            ),
            'guests' => array_map(
                function($guest) {
                    return $guest->dancingFormat();
                },
                $this->guests->all()
            )
        ];
    }
}
