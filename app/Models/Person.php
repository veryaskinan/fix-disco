<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    const TABLE = 'people';
    const MTM_GENRES_TABLE = 'mtm_people_genres';

    protected $table = self::TABLE;

    protected $fillable = ['name','sex'];

    public $timestamps = false;

    /**
     * Defines many-to-many relation with compositions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function genres()
    {
        return $this->belongsToMany('App\Models\Genre', 'mtm_people_genres');
    }

    /**
     * Defines many-to-one relation with clubs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function club()
    {
        return $this->belongsTo('App\Models\Club');
    }

    static function add(array $peopleArray)
    {
        $result = new \App\Http\Result();

        $mtmRelations = [];
        foreach ($peopleArray as $person) {
            $mtmRelations[] = [
                'person_id' => $person->id
            ];
        }

        $peopleArrayForInsert = array_map(
            function($person) {
                unset($person->genres);
                return (array)$person;
            },
            $peopleArray
        );

        DB::transaction(
            function () use ($result, $peopleArrayForInsert, $mtmRelations) {
                DB::table(self::TABLE)->insert($peopleArrayForInsert);
                DB::table(self::MTM_GENRES_TABLE)->insert($mtmRelations);
                $result->success();
            }
        );

        return $result;
    }

    static function remove(array $peopleIdsArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $result->success = DB::table(self::TABLE)
            ->whereIn('id', $peopleIdsArray)
            ->delete()
        ;

        return $result;
    }

    public function short()
    {
        $genres = array_map(function(\App\Models\Genre $genre) {return $genre->name;}, $this->genres->all());
        $genresJson = json_encode($genres);
        $genresShort = str_replace('"','', $genresJson);

        $clubshort = $this->club !== null ? $this->club->short()->data : '';

        return (object)[
            "id" => $this->id,
            "data" => "{$this->name} ({$this->sex}) {$clubshort}, {$genresShort}"
        ];
    }

    public function dancingFormat()
    {
        return (object)[
            'name' => $this->name,
            'sex' => $this->sex,
            'club' => $this->club->short(),
            'genres' => array_map(function(\App\Models\Genre $genre) {return $genre->short();}, $this->genres->all())
        ];
    }
}
