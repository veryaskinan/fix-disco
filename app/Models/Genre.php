<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    const TABLE = 'genres';

    protected $table = self::TABLE;

    public function compositions()
    {
        return $this->hasMany('App\Models\Compositions');
    }

    static function add (array $genresArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $arrayForInsert = array_map(
            function($genre) {
                return (array)$genre;
            },
            $genresArray
        );

        $result->success = DB::table(self::TABLE)->insert($arrayForInsert);

        return $result;
    }

    static function remove (array $genresIdsArray)
    {
        $result = new \stdClass();
        $result->success = false;

        $result->success = DB::table(self::TABLE)
            ->whereIn('id', $genresIdsArray)
            ->delete()
        ;

        return $result;
    }

    public function short() {
        return (object)[
            "id" => $this->id,
            "data" => $this->id . '. ' . $this->name
        ];
    }
}
