<?php

namespace App\Http;


class Result
{
    public $success = false;
    public $error;

    public function __construct__ ()
    {
        $this->error = new \App\Error('Unknown error');
    }

    public function success()
    {
        $this->success = true;
        $this->error = false;
    }

    public function error(\Throwable $error)
    {
        $this->success = false;

        $message = $error->getMessage() . "\n\n" . $error->getFile() . " : " . $error->getLine();

        $this->error = new \App\Error($message);
    }
}
