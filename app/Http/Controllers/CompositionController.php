<?php

namespace App\Http\Controllers;

class CompositionController extends \App\Http\Controllers\Controller
{
    function get()
    {
        $result = new \App\Http\Result();

        try {
            $items = \App\Models\Composition::with('genre')->get();
            $result->success();
            $result->entities = (object)[
                'short' => array_map(function($item){return $item->short();}, $items->all()),
                'full' => $items
            ];
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function add(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->all();
            \App\Models\Composition::add($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function remove(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Models\Composition::remove($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function addToPlaylist(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Services\CompositionService::addToPlaylist($input['compositionsIds'], $input['clubId']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function removeFromPlaylist(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Services\CompositionService::removeFromPlaylist($input['compositionsIds'], $input['clubId']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }
}
