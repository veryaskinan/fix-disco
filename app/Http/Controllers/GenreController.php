<?php

namespace App\Http\Controllers;

class GenreController extends \App\Http\Controllers\Controller
{
    function get()
    {
        $result = new \App\Http\Result();

        try {
            $items = \App\Models\Genre::all();
            $result->success();
            $result->entities = (object)[
                'short' => array_map(function($item){return $item->short();}, $items->all()),
                'full' => $items
            ];
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function add(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->all();
            \App\Models\Genre::Add($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function remove(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Models\Genre::remove($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }
}
