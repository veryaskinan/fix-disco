<?php

namespace App\Http\Controllers;

class ClubController extends \App\Http\Controllers\Controller
{
    function get()
    {
        $result = new \App\Http\Result();

        try {
            $items = \App\Models\Club::with(['compositions', 'guests.genres'])->get();
            $result->success();
            $result->entities = (object)[
                'short' => array_map(function($item){return $item->short();}, $items->all()),
                'full' => $items
            ];
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function add(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Models\Club::add($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function remove(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Models\Club::remove($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function addMusic(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            $clubService = new \App\Services\ClubService();
            $clubService->addMusicToClub($input['clubId'], $input['compositionIds']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception);
        }

        return response()->json($result);
    }

    function removeMusic(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            $clubService = new \App\Services\ClubService();
            $clubService->removeMusicFromClub($input['clubId'], $input['compositionIds']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception);
        }

        return response()->json($result);
    }

    function playMusic()
    {
        $result = new \App\Http\Result();

        try {
            $clubService = new \App\Services\ClubService();
            $danceResult = $clubService->playMusic();
            $result->success();
            $result->danceResult = $danceResult;
        } catch (\Exception $exception) {
            $result->error($exception);
        } catch (\Error $error) {
            $result->error($error);
        }

        return response()->json($result);
    }

}
