<?php

namespace App\Http\Controllers;

class PeopleController extends \App\Http\Controllers\Controller
{
    function get()
    {
        $result = new \App\Http\Result();

        try {
            $items = \App\Models\Person::with('genres')->get();
            $result->success();
            $result->entities = (object)[
                'short' => [],
                'full' => $items->all()
            ];
            foreach ($result->entities->full as $item) {
                $result->entities->short[] = $item->short();
            }
        } catch (\Exception $exception) {
            $result->error($exception);
        }

        return response()->json($result);
    }

    function add(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Services\PeopleService::add($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception);
        }

        return response()->json($result);
    }

    function remove(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Models\Person::remove($input);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception);
        }

        return response()->json($result);
    }

    function enterClub(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Services\PeopleService::enterClub($input['peopleIds'], $input['clubId']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }

    function leaveClub(\Illuminate\Http\Request $request)
    {
        $result = new \App\Http\Result();

        try {
            $input = $request->input();
            \App\Services\PeopleService::leaveClub($input['peopleIds']);
            $result->success();
        } catch (\Exception $exception) {
            $result->error($exception->getMessage());
        }

        return response()->json($result);
    }
}
