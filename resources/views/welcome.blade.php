<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css">

        <script src="http://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>

        <title>Disco</title>

        <!-- Styles -->
        <style>
            html, body {
                height: 100vh;
                margin: 0;
                font-family: arial;
            }
            h1, h2, h3, h4, h5, h6 {
                margin: 0;
            }
            .body {
                display:flex;
                border-right:solid 1px rgba(0,0,0,.1);
            }
            .Content {
                flex-grow:1;
            }
            .mainMenu .title {
                padding:10px 10px 20px 10px;
            }
            .mainMenu a {
                display:block;
                padding:10px;
                cursor: pointer;
            }
            .mainMenu a:hover {
                background: rgba(0,0,0,.1);
            }
            .form-container {
                display:flex;
                flex-direction: column;
            }
            .add-form * {
                margin:10px 0;
            }
            .header {
                border-bottom:solid 1px rgba(0,0,0,.1);
                display:flex;
                padding:20px;
            }
            .entitiy-block {
                margin-right:10px;
                display:flex;
                flex-direction: column;
            }
            .entities-list{
                height:200px;
                min-width:200px;
            }
            textarea {
                width:500px;
                height:300px;
            }
            .example {
                font-size:12px; color: gray;
                padding: 10px 0;
            }
            .fancybox-is-open .fancybox-bg {
                 opacity: .4;
            }
            .button-container {
                display:flex;
            }
            .button-container * {
                flex-grow:1;
            }
            .clubs-tabs {
                display:flex;
                border-bottom:solid 1px rgba(0,0,0,.1);
            }
            .clubs-tabs .club-title {
                padding:10px 20px;
                cursor:pointer;
            }
            .clubs-tabs .club-title:hover,
            .clubs-tabs .club-title.active {
                background: rgba(0,0,0,.1);
            }
            .club-info {
                display: none;
                padding: 20px;
            }
            .club-info.active {
                display: flex;
            }
            .club-info > * {
                margin-right:20px;
            }
            select[name=enterclub],
            select[name=removeFromPlaylist],
            select[name=addToPlaylist] {
                margin: 10px 0;
            }
            #playMusic {
                cursor:pointer;
                border:none;
                border-radius:0;
                background: none;
                padding: 0 20px;
            }
            #playMusic:hover {
                background: rgba(0,255,0,.2);
            }
        </style>
    </head>
    <body>
        <div class="body">
            <div class="Content">
                <div class="header">
                    <div class="entitiy-block genres">
                        <h5>Genres</h5>
                        <select id="genres-list" class="entities-list" name="genres[]" multiple></select>
                        <div class="button-container">
                            <button data-fancybox data-src="#add-genres">Add</button>
                            <button id="remove-genres-button">Remove</button>
                        </div>
                    </div>
                    <div class="entitiy-block people">
                        <h5>People</h5>
                        <select id="people-list" class="entities-list" name="people[]" multiple></select>
                        <div class="button-container">
                            <button data-fancybox data-src="#add-people">Add</button>
                            <button id="remove-people-button">Remove</button>
                            <button data-fancybox data-src="#enter-club">Enter club</button>
                            <button id="leave-club-button">Leave club</button>
                        </div>
                    </div>
                    <div class="entitiy-block compositions">
                        <h5>Compositions</h5>
                        <select id="compositions-list" class="entities-list" name="compositions[]" multiple></select>
                        <div class="button-container">
                            <button data-fancybox data-src="#add-compositions">Add</button>
                            <button id="remove-compositions-button">Remove</button>
                            <button data-fancybox data-src="#add-to-playlist">Add to club's playlist</button>
                            <button data-fancybox data-src="#remove-from-playlist">Remove from club's playlist</button>
                        </div>
                    </div>
                    <div class="entitiy-block clubs">
                        <h5>Clubs</h5>
                        <select id="clubs-list" class="entities-list" name="clubs[]" multiple></select>
                        <div class="button-container">
                            <button data-fancybox data-src="#add-clubs">Add</button>
                            <button id="remove-clubs-button">Remove</button>
                        </div>
                    </div>
                </div>
                <div class="clubsContent">
                    <nav class="clubs-tabs">
                    </nav>
                    <div class="clubs-info">
                    </div>
                </div>
            </div>
            <div style="display: none;" id="add-genres">
                <h3>Add genres</h3>
                <div class="form-container">
                    <label>
                        Type one genre per line<br><br>
                        Example:<br>
                        <div class="example">
                            {"name":"Rock"}<br>
                            {"name":"Jazz"}<br>
                            {"name":"Pop"}<br>
                        </div>
                    </label>
                    <form class="add-form" data-source="genres" id="add-genres-form">
                        <textarea name="genres"></textarea>
                    </form>
                    <input type="submit" value="Add genres" form="add-genres-form">
                </div>
            </div>
            <div style="display: none;" id="add-people">
                <h3>Add people</h3>
                <div class="form-container">
                    <label>
                        Type one person per line<br><br>
                        Example:<br>
                        <div class="example">
                            {"name":"Andrei", "genres":[1], "sex":"male"}<br>
                            {"name":"Dilyara", "genres":[1, 2], "sex":"female"}<br>
                        </div>
                    </label>
                    <form class="add-form" data-source="people" id="add-people-form">
                        <textarea name="people"></textarea>
                    </form>
                    <input type="submit" value="Add people" form="add-people-form">
                </div>
            </div>
            <div style="display: none;" id="enter-club">
                <h3>Enter club</h3>
                <div class="form-container">
                    <form class="enter-club" id="enter-club-form">
                        <label>Which club do you want these people to enter?<br>
                            <select name="enterclub"></select>
                        </label>
                    </form>
                    <input type="submit" value="Enter club" form="enter-club-form">
                </div>
            </div>
            <div style="display: none;" id="add-compositions">
                <h3>Add compositions</h3>
                <div class="form-container">
                    <label>
                        Type one person per line<br><br>
                        Example:<br>
                        <div class="example">
                            {"name":"Composition1", "genre_id":1, "romantic":true}<br>
                            {"name":"Composition2", "genre_id":2, "romantic":false}<br>
                        </div>
                    </label>
                    <form class="add-form" data-source="compositions" id="add-compositions-form">
                        <textarea name="compositions"></textarea>
                    </form>
                    <input type="submit" value="Add compositions" form="add-compositions-form">
                </div>
            </div>
            <div style="display: none;" id="add-to-playlist">
                <h3>Add to playlist</h3>
                <div class="form-container">
                    <form class="add-to-playlist" id="add-to-playlist-form">
                        <label>Which club do you want these compositions to add?<br>
                            <select name="addToPlaylist"></select>
                        </label>
                    </form>
                    <input type="submit" value="Add to playlist" form="add-to-playlist-form">
                </div>
            </div>
            <div style="display: none;" id="remove-from-playlist">
                <h3>Remove from playlist</h3>
                <div class="form-container">
                    <form class="remove-from-playlist" id="remove-from-playlist-form">
                        <label>Which club do you want these compositions to remove from?<br>
                            <select name="removeFromPlaylist"></select>
                        </label>
                    </form>
                    <input type="submit" value="Remove from playlist" form="remove-from-playlist-form">
                </div>
            </div>
            <div style="display: none;" id="add-clubs">
                <h3>Add clubs</h3>
                <div class="form-container">
                    <label>
                        Type one club per line<br><br>
                        Example:<br>
                        <div class="example">
                            {"name":"Club 1"}<br>
                            {"name":"Club 2"}<br>
                        </div>
                    </label>
                    <form class="add-form" data-source="clubs" id="add-clubs-form">
                        <textarea name="clubs"></textarea>
                    </form>
                    <input type="submit" value="Add clubs" form="add-clubs-form">
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    let _GLOBALS = {};

    getEntities();
    addRemoveHandlers();
    clubsTabs();
    enterClubHandler();
    leaveClubHandler();
    addToPlaylistHandler();
    removeFromPlaylistHandler();
    playMusicHandler();

    function getEntities() {
        getAll('genres');
        getAll('people');
        getAll('compositions');
        getAll('clubs');
    }

    function addRemoveHandlers() {
        addEntityHandler('genres');
        addEntityHandler('people');
        addEntityHandler('compositions');
        addEntityHandler('clubs');
        removeEntityHandler('genres');
        removeEntityHandler('people');
        removeEntityHandler('compositions');
        removeEntityHandler('compositions');
        removeEntityHandler('clubs');
    }

    function getAll(type) {
        console.log(`/api/${type}/get`);
        $.ajax({
            url: `/api/${type}/get`,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function(result) {
                if (result.success) {
                    _GLOBALS[type] = result.entities;
                    printAll(type);
                } else {
                    alert(result.error.message);
                }
            }
        });
    }

    function printAll(type) {
        $(`#${type}-list`).empty();
        console.log(_GLOBALS[type]);
        if (typeof _GLOBALS[type] !== 'undefined') {
            _GLOBALS[type].short.forEach(
                entity => {
                    let entityJson = JSON.stringify(entity.data).trim();
                    $(`#${type}-list`).append(`<option value='${entity.id}'>${entityJson}</option>`);
                }
            );
            if (type === 'clubs') {
                $('.clubs-tabs').empty();
                $('.clubs-info').empty();
                $('select[name=enterclub]').empty();
                $('select[name=addToPlaylist]').empty();
                $('select[name=removeFromPlaylist]').empty();
                if (_GLOBALS[type].full.length) {
                    $('.clubs-tabs').append(`<button id='playMusic'>play</button>`);
                    for(let index = 0; index < _GLOBALS[type].full.length; index++) {
                        let club = _GLOBALS[type].full[index];
                        $('.clubs-tabs').append(`<div class='club-title' data-club-id='${club.id}'>${club.name}</div>`);
                        let clubInfo = $(`<div class='club-info' data-club-id='${club.id}'></div>`);

                        let people = $('<div class="club-guests>"><h4>Guests</h4></div>');
                        for (let i = 0; i < club.guests.length; i++) {
                            people.append(club.guests[i].name + '<br>');
                        }
                        clubInfo.append(people);

                        let compositions = $('<div class="club-compositions>"><h4>Compositions</h4></div>');
                        for (let i = 0; i < club.compositions.length; i++) {
                            compositions.append(club.compositions[i].name + '<br>');
                        }
                        clubInfo.append(compositions);

                        $('.clubs-info').append(clubInfo);
                        if (index === 0) {
                            activateClub(club.id);
                        }
                        $('select[name=enterclub]').append(`<option value='${club.id}'>${club.name}</option>`);
                        $('select[name=addToPlaylist]').append(`<option value='${club.id}'>${club.name}</option>`);
                        $('select[name=removeFromPlaylist]').append(`<option value='${club.id}'>${club.name}</option>`);
                    }

                    play();
                }
            }
        }
    }

    function addEntityHandler(type) {
        $(`#add-${type}-form`).on(
            'submit', function () {
                let text = $(this).find(`[name=${type}]`).val();
                let entities = text.split('\n').map(
                    item => JSON.parse(item)
                );
                $.ajax({
                    url: `/api/${type}/add`,
                    type: 'POST',
                    data: JSON.stringify(entities),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            getEntities();
                            $.fancybox.close(`#add-${type}`);
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
                return false;
            }
        );
    }
    function removeEntityHandler(type) {
        $(`#remove-${type}-button`).on(
            'click', function() {
                let entitiesIds = $(this).closest('.entitiy-block').find('.entities-list').val();
                $.ajax({
                    url: `/api/${type}/remove`,
                    type: 'POST',
                    data: JSON.stringify(entitiesIds),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            getEntities();
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
            }
        )
    }
    function enterClubHandler() {
        $(document).on(
            'submit', '#enter-club', function() {
                let data = {};
                data.clubId = $(this).find('select[name=enterclub]').val();
                data.peopleIds = $('#people-list').val();
                $.ajax({
                    url: `/api/people/enter-club`,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            $.fancybox.close('#enter-club');
                            getEntities();
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
                return false;
            }
        );
    }
    function leaveClubHandler() {
        $(document).on(
            'click', '#leave-club-button', function() {
                let data = {peopleIds: $('#people-list').val()};
                $.ajax({
                    url: `/api/people/leave-club`,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            getEntities();
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
                return false;
            }
        );
    }
    function addToPlaylistHandler() {
        $(document).on(
            'submit', '#add-to-playlist-form', function() {
                let data = {};
                data.clubId = $(this).find('select[name=addToPlaylist]').val();
                data.compositionsIds = $('#compositions-list').val();
                $.ajax({
                    url: `/api/compositions/add-to-playlist`,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            $.fancybox.close('#add-to-playlist');
                            getEntities();
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
                return false;
            }
        );
    }
    function removeFromPlaylistHandler() {
        $(document).on(
            'submit', '#remove-from-playlist-form', function() {
                let data = {};
                data.clubId = $(this).find('select[name=removeFromPlaylist]').val();
                data.compositionsIds = $('#compositions-list').val();
                $.ajax({
                    url: `/api/compositions/remove-from-playlist`,
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            $.fancybox.close('#remove-from-playlist');
                            getEntities();
                        } else {
                            alert(result.error.message)
                        }
                    }
                });
                return false;
            }
        );
    }
    function playMusicHandler() {
        $(document).on(
            'click', '#playMusic', function() {
                play();
                return false;
            }
        );
    }

    function play() {
         $(document).find('.clubResult').remove();
         $.ajax({
             url: `/api/clubs/play-music`,
             type: 'POST',
             contentType: 'application/json; charset=utf-8',
             dataType: 'json',
             success: function (result) {
                 if (result.success) {
                     for (let clubIndex in result.danceResult)
                     {
                         let club = result.danceResult[clubIndex];
                         let clubResult = $('<div class="clubResult"></div><br>');
                         for (let compIndex in club.compositions)
                         {
                             let composition = club.compositions[compIndex];
                             let compositionResult = $(`<div class="compositionResult>"><h4>${composition.name}</h4></div><br>`);
                             let compositionResultDance = $(`<div class="compositionResultDance>"><br><h5>Dance</h5></div><br>`);
                             let compositionResultStay= $(`<div class="compositionResultStay>"><h5>Stay</h5></div><br>`);
                             for (let dancingIndex in composition.dancingPeople)
                             {
                                 let dancingResult;
                                 let dancingPerson = composition.dancingPeople[dancingIndex];
                                 if (Array.isArray(dancingPerson)) {
                                     let group = dancingPerson.map(person => person.name);
                                     let groupJoin = group.join(',');
                                     dancingResult = $(`<div class="dancingResult>">[${groupJoin}]</div>`);
                                 } else {
                                     dancingResult = $(`<div class="dancingResult>">${dancingPerson.name}</div>`);
                                 }
                                 compositionResultDance.append(dancingResult);
                             }
                             compositionResult.append(compositionResultDance);
                             for (let statyingIndex in composition.stayingPeople)
                             {
                                 let stayingResult;
                                 let stayingPerson = composition.stayingPeople[statyingIndex];
                                 if (Array.isArray(stayingPerson)) {
                                     let group = stayingPerson.map(person => person.name);
                                     let groupJoin = group.join(',');
                                     stayingResult = $(`<div class="dancingResult>">[${groupJoin}]</div>`);
                                 } else {
                                     stayingResult = $(`<div class="dancingResult>">${stayingPerson.name}</div>`);
                                 }
                                 compositionResultStay.append(stayingResult);
                             }
                             compositionResult.append(compositionResultStay);
                             clubResult.append(compositionResult);
                         }
                         $(`.club-info[data-club-id=${clubIndex}]`).append(clubResult);
                    }
                 } else {
                     alert(result.error.message)
                 }
             }
         });
     }

    function clubsTabs() {
        $(document).on(
            'click', '.club-title', function() {
                let clubId = $(this).data('club-id');
                activateClub(clubId);
            }
        );
    }
    function activateClub(clubId) {
        $('.club-title').removeClass('active');
        $(`.club-title[data-club-id=${clubId}]`).addClass('active');
        $(`.club-info`).removeClass('active');
        $(`.club-info[data-club-id=${clubId}]`).addClass('active');
    }
</script>
